$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 3000
    });
    $('#contacto').on('show.bs.modal', function (e){
        console.log('Apertura modal');
    });

    $('#contacto').on('show.bs.modal', function (e){
        $('#contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('hidden.bs.modal', function (e){
        $('#contactoBtn').prop('disabled', false);
    });
})
